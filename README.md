# Know yourself

Know yourself is an Android app for upgrading your emotional intelligence. This app can recognize your emotion from recorded speech using REST API service. When app fetches recognized emotion it shows you info about that emotion so you can control and be aware of that emotion. App can recognize 6 emotions: anger, fear, disgust, neutral emotion, happiness and sadness. In case that you think that app recognized your emotion wrong, you can choose emotion you think you feel. Also, app saves your emotional states by day and you can see your emotion records trough last 7 days.

To download .apk file, click [here](https://gitlab.com/zagi031/know-yourself/-/blob/main/know-yourself.apk).

Technologies used: Android, Kotlin, RecyclerView, Koin, Navigation Component, Retrofit, Room
