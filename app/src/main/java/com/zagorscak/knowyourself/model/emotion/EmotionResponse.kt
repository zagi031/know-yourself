package com.zagorscak.knowyourself.model.emotion

data class EmotionResponse(
    val classes: List<String>,
    val predictions: List<Double>
)