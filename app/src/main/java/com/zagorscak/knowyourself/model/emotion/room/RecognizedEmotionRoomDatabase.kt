package com.zagorscak.knowyourself.model.emotion.room

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.zagorscak.knowyourself.KnowYourself

@Database(entities = [RecognizedEmotion::class], version = 1)
abstract class RecognizedEmotionRoomDatabase : RoomDatabase() {
    abstract fun recognizedEmotionDao(): RecognizedEmotionDao

    companion object {
        private const val NAME = "recognizedEmotion_database"
        private var instance: RecognizedEmotionRoomDatabase? = null

        fun getInstance(): RecognizedEmotionRoomDatabase {
            if (instance == null) {
                instance = Room.databaseBuilder(
                    KnowYourself.application,
                    RecognizedEmotionRoomDatabase::class.java,
                    NAME
                ).allowMainThreadQueries().fallbackToDestructiveMigration().build()
            }
            return instance as RecognizedEmotionRoomDatabase
        }
    }

}