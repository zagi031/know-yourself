package com.zagorscak.knowyourself.model.emotion.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface RecognizedEmotionDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveRecognizedEmotion(recognizedEmotion: RecognizedEmotion)

    @Query("SELECT * FROM recognizedEmotions")
    fun getAllRecognizedEmotions(): List<RecognizedEmotion>

    @Query("DELETE FROM recognizedEmotions")
    suspend fun deleteAllRecognizedEmotions()
}