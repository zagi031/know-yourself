package com.zagorscak.knowyourself.model.emotion

import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface EmotionsApi {
    @Multipart
    @POST("predict")
    suspend fun getEmotion(@Part audioFilePart: MultipartBody.Part): Response<EmotionResponse>
}