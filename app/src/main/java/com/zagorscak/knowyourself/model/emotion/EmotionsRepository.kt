package com.zagorscak.knowyourself.model.emotion

import androidx.lifecycle.LiveData
import com.zagorscak.knowyourself.model.Emotion
import com.zagorscak.knowyourself.model.SingleLiveEvent
import com.zagorscak.knowyourself.model.emotion.room.RecognizedEmotion
import com.zagorscak.knowyourself.model.emotion.room.RecognizedEmotionDao
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.util.*

class EmotionsRepository(
    private val emotionsApi: EmotionsApi,
    private val recognizedEmotionDao: RecognizedEmotionDao
) {
    companion object {
        const val EMOTIONS_BASE_URL = "https://mzagorscak.pythonanywhere.com/api/"
    }

    private val _errorMessage = SingleLiveEvent<String>()
    val errorMessage: LiveData<String> = _errorMessage

    suspend fun recognizeEmotion(audioFile: File): String? {
        return try {
            val body = RequestBody.create(MediaType.parse("multipart/form-data"), audioFile)
            val part = MultipartBody.Part.createFormData("audioFile", "audioFile", body)
            val emotionsResponse = emotionsApi.getEmotion(part)
            if (emotionsResponse.isSuccessful) {
                val result = emotionsResponse.body()!!
                val index = result.predictions.indexOf(result.predictions.maxOrNull())
                result.classes[index]
            } else null
        } catch (t: Throwable) {
            _errorMessage.postValue(t.message)
            null
        }
    }

    suspend fun saveRecognizedEmotion(emotion: Emotion) {
        val recognizedEmotion = RecognizedEmotion(
            emotion.name,
            emotion.emoticonDrawableResId,
            Calendar.getInstance().timeInMillis
        )
        recognizedEmotionDao.saveRecognizedEmotion(recognizedEmotion)
    }

    fun getAllSavedRecognizedEmotions() = recognizedEmotionDao.getAllRecognizedEmotions()
}