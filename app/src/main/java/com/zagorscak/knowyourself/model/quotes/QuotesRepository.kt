package com.zagorscak.knowyourself.model.quotes

import androidx.lifecycle.LiveData
import com.zagorscak.knowyourself.model.SingleLiveEvent

class QuotesRepository(private val quotesApi: QuotesApi) {

    companion object {
        const val QUOTES_BASE_URL = "https://www.stands4.com/services/v2/"
        const val QUOTES_UID = "10169"
        const val QUOTES_TOKENID = "IrO2il46cPhd0iUq"
    }

    private val _errorMessage = SingleLiveEvent<String>()
    val errorMessage: LiveData<String> = _errorMessage

    suspend fun getQuote(query: String): Quote? {
        return try {
            val quoteResponse = quotesApi.getQuotes(query)
            if (quoteResponse.isSuccessful) {
                quoteResponse.body()?.result?.random()
            } else null
        } catch (t: Throwable) {
            _errorMessage.postValue(t.message)
            null
        }
    }
}