package com.zagorscak.knowyourself.model.images

import com.zagorscak.knowyourself.model.images.ImagesRepository.Companion.IMAGES_API_KEY
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface ImagesApi {

    @Headers("Authorization: $IMAGES_API_KEY")
    @GET("search")
    suspend fun getImages(
        @Query("query") query: String,
        @Query("size") size: String = "small",
        @Query("per_page") perPage: String = "80"
    ): Response<ImagesResponse>
}