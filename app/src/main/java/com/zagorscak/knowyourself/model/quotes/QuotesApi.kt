package com.zagorscak.knowyourself.model.quotes

import com.zagorscak.knowyourself.model.quotes.QuotesRepository.Companion.QUOTES_TOKENID
import com.zagorscak.knowyourself.model.quotes.QuotesRepository.Companion.QUOTES_UID
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface QuotesApi {
    @GET("quotes.php")
    suspend fun getQuotes(
        @Query("query") query: String,
        @Query("uid") uid: String = QUOTES_UID,
        @Query("tokenid") tokenId: String = QUOTES_TOKENID,
        @Query("searchtype") searchType: String = "SEARCH",
        @Query("format") format: String = "json"
    ) : Response<QuotesResponse>
}