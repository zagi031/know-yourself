package com.zagorscak.knowyourself.model.emotion.room

import android.annotation.SuppressLint
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.text.SimpleDateFormat
import java.util.*

@Entity(tableName = "recognizedEmotions")
data class RecognizedEmotion(
    val emotionName: String,
    val iconResId: Int,
    @PrimaryKey val timeOfRecognition: Long //in milliseconds
) {
    @SuppressLint("SimpleDateFormat")
    fun getDayNumber(): String {
        val date = Date(timeOfRecognition)
        return SimpleDateFormat("dd", Locale.ENGLISH).format(date)
    }

    @SuppressLint("SimpleDateFormat")
    fun getMonthName(): String {
        val date = Date(timeOfRecognition)
        return SimpleDateFormat("MMMM", Locale.ENGLISH).format(date)
    }
}