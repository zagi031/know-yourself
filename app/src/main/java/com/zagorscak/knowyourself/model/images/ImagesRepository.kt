package com.zagorscak.knowyourself.model.images

import androidx.lifecycle.LiveData
import com.zagorscak.knowyourself.model.SingleLiveEvent

class ImagesRepository(private val imagesApi: ImagesApi) {

    companion object {
        const val IMAGES_BASE_URL = "https://api.pexels.com/v1/"
        const val IMAGES_API_KEY = "563492ad6f917000010000017c7ea8b97ecd4aa582734e44c947a229"
    }

    private val _errorMessage = SingleLiveEvent<String>()
    val errorMessage: LiveData<String> = _errorMessage

    suspend fun getImageURL(searchTags: String): String? {
        return try {
            val imagesResponse = imagesApi.getImages(searchTags)
            if (imagesResponse.isSuccessful) {
                imagesResponse.body()?.photos?.random()?.src?.large
            } else null
        } catch (t: Throwable) {
            _errorMessage.postValue(t.message)
            null
        }
    }
}