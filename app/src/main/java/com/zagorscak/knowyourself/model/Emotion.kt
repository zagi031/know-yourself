package com.zagorscak.knowyourself.model

import com.zagorscak.knowyourself.R

enum class Emotion(
    val similarWordsArrayResId: Int,
    val sensationsResId: Int,
    val messageResId: Int,
    val helpMessageResId: Int,
    val APIkeywords: List<String>,
    val solfeggioFrequency: Int,
    val solfeggioFrequencyAudioResId: Int,
    val emoticonDrawableResId: Int
) {
    Anger(
        R.array.angerSimilarWords,
        R.string.angerSensations,
        R.string.angerMessage,
        R.string.angerHelpMessage,
        listOf(), 852, R.raw.anger_852hz, R.drawable.anger
    ),

    Disgust(
        R.array.disgustSimilarWords,
        R.string.disgustSensations,
        R.string.disgustMessage,
        R.string.disgustHelpMessage,
        listOf(), 639, R.raw.disgust_639hz, R.drawable.disgust
    ),

    Fear(
        R.array.fearSimilarWords,
        R.string.fearSensations,
        R.string.fearMessage,
        R.string.fearHelpMessage,
        listOf(), 396, R.raw.fear_396hz, R.drawable.fear
    ),

    Happiness(
        R.array.happinessSimilarWords,
        R.string.happinessSensations,
        R.string.happinessMessage,
        R.string.happinessHelpMessage,
        listOf(), 528, R.raw.happiness_528hz, R.drawable.happy
    ),

    Neutral(
        R.array.neutralSimilarWords,
        R.string.neutralSensations,
        R.string.neutralMessage,
        R.string.neutralHelpMessage,
        listOf(), 741, R.raw.neutral_741hz, R.drawable.neutral
    ),

    Sadness(
        R.array.sadnessSimilarWords,
        R.string.sadnessSensations,
        R.string.sadnessMessage,
        R.string.sadnessHelpMessage,
        listOf(), 174, R.raw.sadness_174hz, R.drawable.sad
    )
}

fun getEmotionFromAPIKeyword(emotionAPIKeyword: String): Emotion {
    return when (emotionAPIKeyword) {
        "anger" -> Emotion.Anger
        "disgust" -> Emotion.Disgust
        "fear" -> Emotion.Fear
        "happy" -> Emotion.Happiness
        "sad" -> Emotion.Sadness
        else -> Emotion.Neutral
    }
}

