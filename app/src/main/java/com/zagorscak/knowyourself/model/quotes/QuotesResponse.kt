package com.zagorscak.knowyourself.model.quotes

data class QuotesResponse(
    val result: List<Quote>
)

data class Quote(
    val author: String,
    val quote: String
)