package com.zagorscak.knowyourself.services

import android.app.ActivityManager
import android.app.Notification
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.Uri
import android.os.IBinder
import androidx.core.app.NotificationCompat
import com.zagorscak.knowyourself.KnowYourself
import com.zagorscak.knowyourself.KnowYourself.Companion.CHANNEL_ID
import com.zagorscak.knowyourself.R
import com.zagorscak.knowyourself.model.Emotion


class MediaPlayerService : Service() {
    companion object {
        const val SERVICEID = 369
        const val INTENT_EXTRA_EMOTION_KEY = "emotionExtraKey"
        const val ACTION_STOP = "actionStop"

        fun isServiceRunning(): Boolean {
            val manager = KnowYourself.application.getSystemService(ACTIVITY_SERVICE) as ActivityManager?
            for (service in manager!!.getRunningServices(Int.MAX_VALUE)) {
                if ("com.zagorscak.knowyourself.services.MediaPlayerService" == service.service.className) {
                    return true
                }
            }
            return false
        }
    }

    private var mediaPlayer = createMediaPlayer()

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null && intent.action == ACTION_STOP) {
            stopForeground(true)
            stopSelf()
        } else {
            val stopIntent =
                Intent(KnowYourself.application.applicationContext, MediaPlayerService::class.java)
            stopIntent.action = ACTION_STOP
            val stopPendingIntent = PendingIntent.getService(
                KnowYourself.application.applicationContext,
                0, stopIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
            val emotion = intent?.extras?.getSerializable(INTENT_EXTRA_EMOTION_KEY) as Emotion
            setMediaPlayerDataSource(emotion.solfeggioFrequencyAudioResId)
            val notification: Notification = NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Know yourself media player")
                .setContentText("Feeling ${emotion.name.lowercase()} - playing ${emotion.solfeggioFrequency} Hz Solfeggio Frequency")
                .setSmallIcon(emotion.emoticonDrawableResId)
                .addAction(R.drawable.ic_stop, "Stop playing", stopPendingIntent)
                .build()

            startForeground(SERVICEID, notification)
            startPlaying()
        }

        return START_NOT_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        stopPlaying()
    }

    private fun createMediaPlayer(): MediaPlayer {
        return MediaPlayer().apply {
            isLooping = true
            setAudioAttributes(
                AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .build()
            )
        }
    }

    private fun setMediaPlayerDataSource(sourceResId: Int) {
        mediaPlayer.setDataSource(
            KnowYourself.application.applicationContext,
            Uri.parse("android.resource://${KnowYourself.application.applicationContext.packageName}/$sourceResId")
        )
    }

    private fun startPlaying() {
        mediaPlayer.apply {
            prepareAsync()
            setOnPreparedListener {
                it.start()
            }
        }
    }

    private fun stopPlaying() {
        mediaPlayer.stop()
        mediaPlayer.release()
        mediaPlayer = createMediaPlayer()
    }
}