package com.zagorscak.knowyourself.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.zagorscak.knowyourself.model.emotion.EmotionsRepository
import com.zagorscak.knowyourself.ui.adapters.EmotionsByDay
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import java.text.SimpleDateFormat
import java.util.*

class EmotionRecordsViewModel(private val emotionsRepository: EmotionsRepository) : ViewModel() {

    fun getSavedRecognizedEmotions(): LiveData<List<EmotionsByDay>> {
        val result = viewModelScope.async(Dispatchers.IO) {
            emotionsRepository.getAllSavedRecognizedEmotions()
        }
        return runBlocking {
            val allEmotions = result.await()
            val emotionsByDays = mutableListOf<EmotionsByDay>()
            (0..6).forEach {
                val timeInMillis =
                    Calendar.getInstance().apply { add(Calendar.DATE, -it) }.timeInMillis
                val dayNumber = dateToFormattedString("dd", timeInMillis)
                val monthName = dateToFormattedString("MMMM", timeInMillis)
                val emotions = allEmotions.filter {
                    dayNumber == dateToFormattedString("dd", it.timeOfRecognition) &&
                            monthName == dateToFormattedString("MMMM", it.timeOfRecognition)
                }
                emotionsByDays.add(EmotionsByDay(dayNumber, monthName, emotions))
            }
            MutableLiveData(emotionsByDays)
        }
    }

    private fun dateToFormattedString(formatPattern: String, timeInMillis: Long) =
        SimpleDateFormat(formatPattern, Locale.ENGLISH).format(Date(timeInMillis))
}