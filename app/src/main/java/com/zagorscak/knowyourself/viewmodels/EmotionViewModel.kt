package com.zagorscak.knowyourself.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.zagorscak.knowyourself.KnowYourself
import com.zagorscak.knowyourself.model.Emotion
import com.zagorscak.knowyourself.model.SingleLiveEvent
import com.zagorscak.knowyourself.model.emotion.EmotionsRepository
import com.zagorscak.knowyourself.model.getEmotionFromAPIKeyword
import com.zagorscak.knowyourself.model.images.ImagesRepository
import com.zagorscak.knowyourself.model.quotes.Quote
import com.zagorscak.knowyourself.model.quotes.QuotesRepository
import java.io.File

class EmotionViewModel(
    private val quotesRepository: QuotesRepository,
    private val imagesRepository: ImagesRepository,
    private val emotionsRepository: EmotionsRepository
) : ViewModel() {
    private val _errorMessage = SingleLiveEvent<String>()
    val errorMessage: LiveData<String>

    init {
        errorMessage = MediatorLiveData<String>().apply {
            addSource(quotesRepository.errorMessage) {
                this.postValue(it)
            }
            addSource(imagesRepository.errorMessage) {
                this.postValue(it)
            }
            addSource(emotionsRepository.errorMessage) {
                this.postValue(it)
            }
            addSource(_errorMessage) {
                this.postValue(it)
            }
        }
    }

    var isQuoteFetched = false
        private set
    private val _quote = MutableLiveData<String>()
    val quote: LiveData<String> = _quote
    suspend fun getQuote(emotion: Emotion) {
        if (!isQuoteFetched) {
            val quote = quotesRepository.getQuote("${emotion.name} emotion")
            if (quote != null) {
                isQuoteFetched = true
                _quote.postValue(formatQuote(quote))
            } else _errorMessage.postValue("Some error occurred")
        }
    }

    private fun formatQuote(quote: Quote) =
        "${quote.quote}\n- ${quote.author}"

    var isImageFetched = false
        private set
    private val _imageURL = MutableLiveData<String>()
    val imageURL: LiveData<String> = _imageURL
    suspend fun getImage(emotion: Emotion) {
        if (!isImageFetched) {
            val imageURL = imagesRepository.getImageURL(emotion.name)
            if (imageURL != null) {
                isImageFetched = true
                _imageURL.postValue(imageURL.toString())
            } else _errorMessage.postValue("Some error occurred")
        }
    }

    var isEmotionFetched = false
        private set
    private val _emotion = MutableLiveData<Emotion>()
    val emotion: LiveData<Emotion> = _emotion
    private var __emotion: Emotion? = null
    fun getRecognizedEmotion() = __emotion
    suspend fun getEmotion() {
        val filepath =
            KnowYourself.application.applicationContext.externalCacheDir?.path + "/audio/track.wav"
        if (!isEmotionFetched) {
            val emotionKeyword = emotionsRepository.recognizeEmotion(File(filepath))
            if (emotionKeyword != null) {
                isEmotionFetched = true
                val emotion = getEmotionFromAPIKeyword(emotionKeyword)
                __emotion = emotion
                _showCheckDialogForRecognizedEmotion.postValue(emotion.name)
                _emotion.postValue(emotion)
                getImage(emotion)
                getQuote(emotion)
            } else _errorMessage.postValue("Some error occurred")
        }
    }

    private val _showCheckDialogForRecognizedEmotion = SingleLiveEvent<String>()
    val showCheckDialogForRecognizedEmotion: LiveData<String> = _showCheckDialogForRecognizedEmotion

    suspend fun changeEmotion(emotion: Emotion) {
        isImageFetched = false
        isQuoteFetched = false
        getImage(emotion)
        getQuote(emotion)
        _emotion.postValue(emotion)
        __emotion = emotion
        saveRecognizedEmotion()
    }

    fun resetEmotionFetchedState() {
        isEmotionFetched = false
        isQuoteFetched = false
        isImageFetched = false
    }

    suspend fun saveRecognizedEmotion() =
        __emotion?.let { emotionsRepository.saveRecognizedEmotion(it) }

}