package com.zagorscak.knowyourself.viewmodels

import android.media.AudioFormat
import android.media.MediaMetadataRetriever
import android.media.MediaRecorder
import androidx.lifecycle.ViewModel
import com.zagorscak.knowyourself.KnowYourself
import com.zagorscak.knowyourself.R
import omrecorder.*
import java.io.File

class RecorderViewModel : ViewModel() {
    private var recorder: Recorder = instantiateRecorder()

    companion object {
        private const val minRecordedAudioDuration = 500    // milis
    }

    private fun instantiateRecorder() =
        OmRecorder.wav(PullTransport.Noise(mic()), file())

    private fun file(): File {
        val cachePath = File(KnowYourself.application.applicationContext.externalCacheDir, "audio/")
        cachePath.mkdirs()
        return File(cachePath, "track.wav")
    }

    private fun mic() = PullableSource.NoiseSuppressor(
        PullableSource.Default(
            AudioRecordConfig.Default(
                MediaRecorder.AudioSource.MIC, AudioFormat.ENCODING_PCM_16BIT,
                AudioFormat.CHANNEL_IN_MONO, 44100
            )
        )
    )

    fun startRecording() = recorder.startRecording()

    fun stopRecording() {
        recorder.stopRecording()
        recorder = instantiateRecorder()
    }

    fun hasAudioReachedLowestDuration() = getAudioDuration() > minRecordedAudioDuration

    // in milis
    private fun getAudioDuration(): Int {
        val filePath = File(
            File(KnowYourself.application.applicationContext.externalCacheDir, "audio/"),
            "track.wav"
        )
        val mmr = MediaMetadataRetriever()
        mmr.setDataSource(filePath.toString())
        return Integer.parseInt(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)!!)
    }

    fun getRandomQuestion() =
        KnowYourself.application.resources.getStringArray(R.array.questions).random()!!
}