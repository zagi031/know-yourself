package com.zagorscak.knowyourself.di

import com.zagorscak.knowyourself.model.emotion.EmotionsApi
import com.zagorscak.knowyourself.model.emotion.EmotionsRepository
import com.zagorscak.knowyourself.model.emotion.EmotionsRepository.Companion.EMOTIONS_BASE_URL
import com.zagorscak.knowyourself.model.emotion.room.RecognizedEmotionRoomDatabase
import com.zagorscak.knowyourself.model.images.ImagesApi
import com.zagorscak.knowyourself.model.images.ImagesRepository
import com.zagorscak.knowyourself.model.images.ImagesRepository.Companion.IMAGES_BASE_URL
import com.zagorscak.knowyourself.model.quotes.QuotesApi
import com.zagorscak.knowyourself.model.quotes.QuotesRepository
import com.zagorscak.knowyourself.model.quotes.QuotesRepository.Companion.QUOTES_BASE_URL
import com.zagorscak.knowyourself.viewmodels.EmotionRecordsViewModel
import com.zagorscak.knowyourself.viewmodels.EmotionViewModel
import com.zagorscak.knowyourself.viewmodels.RecorderViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val viewModelModule = module {
    viewModel { RecorderViewModel() }
    viewModel { EmotionViewModel(get(), get(), get()) }
    viewModel { EmotionRecordsViewModel(get()) }

    single {
        Retrofit.Builder().baseUrl(QUOTES_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create()).build()
            .create(QuotesApi::class.java)
    }
    single { QuotesRepository(get()) }
    single {
        Retrofit.Builder().baseUrl(IMAGES_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create()).build()
            .create(ImagesApi::class.java)
    }
    single { ImagesRepository(get()) }
    single {
        Retrofit.Builder().baseUrl(EMOTIONS_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create()).build()
            .create(EmotionsApi::class.java)
    }
    single { RecognizedEmotionRoomDatabase.getInstance().recognizedEmotionDao() }
    single { EmotionsRepository(get(), get()) }
}

