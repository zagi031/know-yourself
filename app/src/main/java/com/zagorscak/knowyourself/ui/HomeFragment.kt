package com.zagorscak.knowyourself.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.zagorscak.knowyourself.R
import com.zagorscak.knowyourself.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            btnShowRecorderFragment.setOnClickListener {
                findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToRecorderFragment())
            }
            btnShowEmotionRecordsFragment.setOnClickListener {
                findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToEmotionRecordsFragment())
            }
        }
    }
}