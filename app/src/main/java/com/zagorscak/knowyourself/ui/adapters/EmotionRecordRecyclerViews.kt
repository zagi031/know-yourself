package com.zagorscak.knowyourself.ui.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.zagorscak.knowyourself.KnowYourself
import com.zagorscak.knowyourself.R
import com.zagorscak.knowyourself.databinding.RvDayHolderBinding
import com.zagorscak.knowyourself.databinding.RvSavedEmotionHolderBinding
import com.zagorscak.knowyourself.databinding.RvZeroEmotionsDayHolderBinding
import com.zagorscak.knowyourself.model.emotion.room.RecognizedEmotion
import java.text.SimpleDateFormat
import java.util.*

data class EmotionsByDay(val day: String, val month: String, val emotions: List<RecognizedEmotion>)

class DaysRecyclerViewAdapter : RecyclerView.Adapter<DayViewHolder>() {

    private val emotionsByDay = mutableListOf<EmotionsByDay>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DayViewHolder(
            RvDayHolderBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun getItemCount() = emotionsByDay.size

    @SuppressLint("NotifyDataSetChanged")
    fun setEmotionsByDay(emotionsByDay: List<EmotionsByDay>) {
        this.emotionsByDay.clear()
        this.emotionsByDay.addAll(emotionsByDay)
        this.notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: DayViewHolder, position: Int) =
        holder.bind(emotionsByDay[position])

}

class EmptyRecyclerViewNoteHolder(private val binding: RvZeroEmotionsDayHolderBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(message: String) {
        binding.tvEmptyRvNote.text = message
    }
}

class DayViewHolder(private val binding: RvDayHolderBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(emotionsByDay: EmotionsByDay) {
        binding.apply {
            tvDay.text = emotionsByDay.day
            tvMonth.text = emotionsByDay.month
            rvDays.layoutManager =
                LinearLayoutManager(KnowYourself.application, RecyclerView.VERTICAL, false)
            rvDays.adapter = EmotionsRecyclerViewAdapter(emotionsByDay.emotions)
        }
    }
}

class EmotionsRecyclerViewAdapter(private val emotions: List<RecognizedEmotion>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        private const val NON_EMPTY_RECYCLERVIEW_TYPE = 100
        private const val EMPTY_RECYCLERVIEW_TYPE = 200
    }

    override fun getItemViewType(position: Int) =
        if (this.emotions.isNotEmpty()) NON_EMPTY_RECYCLERVIEW_TYPE else EMPTY_RECYCLERVIEW_TYPE


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == NON_EMPTY_RECYCLERVIEW_TYPE) EmotionViewHolder(
            RvSavedEmotionHolderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        ) else EmptyRecyclerViewNoteHolder(
            RvZeroEmotionsDayHolderBinding.inflate(
                LayoutInflater.from(
                    parent.context
                ), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == NON_EMPTY_RECYCLERVIEW_TYPE) {
            (holder as EmotionViewHolder).bind(emotions[position])
        } else (holder as EmptyRecyclerViewNoteHolder).bind(holder.itemView.context.getString(R.string.label_none_emotions_captured))
    }


    override fun getItemCount() = if (emotions.isNotEmpty()) emotions.size else 1

}

class EmotionViewHolder(private val binding: RvSavedEmotionHolderBinding) :
    RecyclerView.ViewHolder(binding.root) {
    @SuppressLint("SimpleDateFormat")
    fun bind(emotion: RecognizedEmotion) {
        binding.apply {
            tvEmotionName.text = emotion.emotionName
            tvEmotionCaptured.text =
                SimpleDateFormat("HH:mm").format(Date(emotion.timeOfRecognition))
            ivEmoticon.setImageResource(emotion.iconResId)
        }
    }
}
