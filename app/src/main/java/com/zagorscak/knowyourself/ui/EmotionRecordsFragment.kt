package com.zagorscak.knowyourself.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.zagorscak.knowyourself.databinding.FragmentEmotionRecordsBinding
import com.zagorscak.knowyourself.ui.adapters.DaysRecyclerViewAdapter
import com.zagorscak.knowyourself.viewmodels.EmotionRecordsViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class EmotionRecordsFragment : Fragment() {
    private lateinit var binding: FragmentEmotionRecordsBinding
    private val viewModel by viewModel<EmotionRecordsViewModel>()
    private val adapter = DaysRecyclerViewAdapter()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentEmotionRecordsBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView()
        viewModel.getSavedRecognizedEmotions().observe(viewLifecycleOwner, {
            adapter.setEmotionsByDay(it)
        })

    }

    private fun setupRecyclerView() {
        binding.rvDays.apply {
            layoutManager = LinearLayoutManager(
                this@EmotionRecordsFragment.context,
                RecyclerView.HORIZONTAL,
                true
            )
            adapter = this@EmotionRecordsFragment.adapter
        }
        PagerSnapHelper().attachToRecyclerView(binding.rvDays)
    }
}