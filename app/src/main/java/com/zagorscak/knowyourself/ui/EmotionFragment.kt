package com.zagorscak.knowyourself.ui

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.squareup.picasso.Picasso
import com.zagorscak.knowyourself.R
import com.zagorscak.knowyourself.databinding.DialogPickEmotionBinding
import com.zagorscak.knowyourself.databinding.FragmentEmotionBinding
import com.zagorscak.knowyourself.model.Emotion
import com.zagorscak.knowyourself.viewmodels.EmotionViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class EmotionFragment : Fragment() {

    private lateinit var binding: FragmentEmotionBinding
    private val viewModel by sharedViewModel<EmotionViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentEmotionBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            scrollview.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
            fcvBottomSheet.visibility = View.GONE
            BottomSheetBehavior.from(binding.fcvBottomSheet).apply {
                peekHeight = resources.getDimensionPixelSize(R.dimen.bottomSheet_peekHeight)
                this.state = BottomSheetBehavior.STATE_COLLAPSED
            }
        }

        viewModel.errorMessage.observe(viewLifecycleOwner, { showToast(it) })
        viewModel.imageURL.observe(viewLifecycleOwner, {
            Picasso.get().load(it).into(binding.ivImage)
            hideProgressBar()
        })
        viewModel.quote.observe(
            viewLifecycleOwner,
            {
                binding.tvQuote.text = it
                hideProgressBar()
            })
        viewModel.emotion.observe(
            viewLifecycleOwner,
            {
                showRecognizedEmotionUI(it)
                hideProgressBar()
            })
        lifecycleScope.launch(Dispatchers.IO) { viewModel.getEmotion() }
        viewModel.showCheckDialogForRecognizedEmotion.observe(
            viewLifecycleOwner,
            { showRecognizedEmotionDialog(it) })
    }

    private fun hideProgressBar() {
        if (viewModel.isEmotionFetched && viewModel.isImageFetched && viewModel.isQuoteFetched) {
            binding.scrollview.visibility = View.VISIBLE
            binding.progressBar.visibility = View.GONE
            binding.fcvBottomSheet.visibility = View.VISIBLE
            YoYo.with(Techniques.SlideInUp).playOn(binding.fcvBottomSheet)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun showRecognizedEmotionUI(emotion: Emotion) {
        val similarWordsArray = resources.getStringArray(emotion.similarWordsArrayResId)
        var similarWords = "( "
        similarWordsArray.forEachIndexed { index, it ->
            similarWords += if (index < similarWordsArray.lastIndex) {
                "$it, "
            } else "$it )"
        }
        binding.apply {
            tvRecognizedEmotion.text = emotion.name
            tvSimilarWords.text = similarWords
            tvSensations.text =
                Html.fromHtml("<b>Typical sensations:</b><br/>${resources.getString(emotion.sensationsResId)}")
            tvMessage.text = Html.fromHtml(
                "<b>What is ${emotion.name.lowercase()} telling you?</b><br/>${
                    resources.getString(emotion.messageResId)
                }"
            )
            tvHelpMessage.text = Html.fromHtml(
                "<b>How can ${emotion.name.lowercase()} help you?</b><br/>${
                    resources.getString(emotion.helpMessageResId)
                }"
            )
        }
    }

    private fun showRecognizedEmotionDialog(emotion: String) {
        val dialog =
            AlertDialog.Builder(context).setTitle("Recognized emotion").setCancelable(false)
                .setMessage("I recognized that you feel ${emotion.lowercase()}. Did I recognized correctly?")
                .setPositiveButton("Yes") { dialogInterface, _ ->
                    dialogInterface.dismiss()
                    lifecycleScope.launch(Dispatchers.IO) { viewModel.saveRecognizedEmotion() }
                }.setNegativeButton("No") { _, _ ->
                    showChangeEmotionDialog()
                }.show()

        dialog.getButton(Dialog.BUTTON_POSITIVE).apply {
            setBackgroundColor(resources.getColor(R.color.blue_900))
            setTextColor(resources.getColor(R.color.white))
        }
        dialog.getButton(Dialog.BUTTON_NEGATIVE).setTextColor(resources.getColor(R.color.blue_900))
    }

    @SuppressLint("InflateParams")
    private fun showChangeEmotionDialog() {
        val builder = AlertDialog.Builder(context)
        val view = DialogPickEmotionBinding.inflate(layoutInflater)
        builder.setView(view.root)
        val dialog = builder.create()
        dialog.window?.setBackgroundDrawableResource(R.drawable.dialog_pick_emotion_background)
        dialog.setCancelable(false)
        dialog.show()
        view.apply {
            ivAnger.setOnClickListener {
                lifecycleScope.launch(Dispatchers.IO) { viewModel.changeEmotion(Emotion.Anger) }
                dialog.dismiss()
            }
            ivDisgust.setOnClickListener {
                lifecycleScope.launch(Dispatchers.IO) { viewModel.changeEmotion(Emotion.Disgust) }
                dialog.dismiss()
            }
            ivFear.setOnClickListener {
                lifecycleScope.launch(Dispatchers.IO) { viewModel.changeEmotion(Emotion.Fear) }
                dialog.dismiss()
            }
            ivHappy.setOnClickListener {
                lifecycleScope.launch(Dispatchers.IO) { viewModel.changeEmotion(Emotion.Happiness) }
                dialog.dismiss()
            }
            ivNeutral.setOnClickListener {
                lifecycleScope.launch(Dispatchers.IO) { viewModel.changeEmotion(Emotion.Neutral) }
                dialog.dismiss()
            }
            ivSad.setOnClickListener {
                lifecycleScope.launch(Dispatchers.IO) { viewModel.changeEmotion(Emotion.Sadness) }
                dialog.dismiss()
            }
        }
    }

    private fun showToast(message: String) =
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
}