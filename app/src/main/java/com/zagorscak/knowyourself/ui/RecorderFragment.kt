package com.zagorscak.knowyourself.ui

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.zagorscak.knowyourself.KnowYourself
import com.zagorscak.knowyourself.R
import com.zagorscak.knowyourself.databinding.FragmentRecorderBinding
import com.zagorscak.knowyourself.viewmodels.EmotionViewModel
import com.zagorscak.knowyourself.viewmodels.RecorderViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import pub.devrel.easypermissions.EasyPermissions

class RecorderFragment : Fragment() {
    private lateinit var binding: FragmentRecorderBinding
    private val viewModel by viewModel<RecorderViewModel>()
    private val sharedViewModel by sharedViewModel<EmotionViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRecorderBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            tvQuestion.text = viewModel.getRandomQuestion()

            val btnAnim = YoYo.with(Techniques.Pulse).repeat(YoYo.INFINITE).duration(1500)
            val pivot = resources.getDimension(R.dimen.progBarDimen) / 2
            val progBarAnim =
                YoYo.with(Techniques.Pulse).pivot(pivot, pivot).repeat(YoYo.INFINITE).duration(1500)
            var btnAnimExe: YoYo.YoYoString? = null
            var progBarAnimExe: YoYo.YoYoString? = null
            btnRecord.setOnTouchListener { view, motionEvent ->
                if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                    if (hasRecordPermissions() && hasStoragePermissions()) {
                        progressBarBtn.visibility = View.VISIBLE
                        btnAnimExe = btnAnim.playOn(view)
                        progBarAnimExe = progBarAnim.playOn(binding.progressBarBtn)
                        viewModel.startRecording()
                    } else {
                        showToast("You need to allow this application to record and access external storage")
                    }
                }
                if (motionEvent.action == MotionEvent.ACTION_UP) {
                    btnAnimExe?.stop()
                    progBarAnimExe?.stop()
                    progressBarBtn.visibility = View.GONE
                    if (hasRecordPermissions() && hasStoragePermissions()) {
                        viewModel.stopRecording()
                    }
                    if (viewModel.hasAudioReachedLowestDuration()) {
                        showDialogOnRecordedVoice()
                    } else showToast("To recognize your current emotion we need some longer voice record.")
                }
                true
            }
        }
    }

    private fun hasRecordPermissions() = EasyPermissions.hasPermissions(
        KnowYourself.application,
        Manifest.permission.RECORD_AUDIO
    )

    private fun hasStoragePermissions() = EasyPermissions.hasPermissions(
        KnowYourself.application,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    private fun showDialogOnRecordedVoice() {
        val dialog = AlertDialog.Builder(context).setTitle("Alert")
            .setMessage("You just recorded your voice. Do you want to proceed to emotion recognition with this record?")
            .setPositiveButton(
                "Continue"
            ) { _, _ ->
                sharedViewModel.resetEmotionFetchedState()
                findNavController().navigate(RecorderFragmentDirections.actionRecorderFragmentToEmotionFragment())
            }.setNeutralButton("Rerecord") { dialogInterface, _ ->
                dialogInterface.cancel()
            }.show()

        dialog.getButton(Dialog.BUTTON_POSITIVE).apply {
            setBackgroundColor(resources.getColor(R.color.blue_900))
            setTextColor(resources.getColor(R.color.white))
        }
        dialog.getButton(Dialog.BUTTON_NEGATIVE).setTextColor(resources.getColor(R.color.blue_900))
    }

    private fun showToast(message: String) =
        Toast.makeText(this.activity, message, Toast.LENGTH_SHORT).show()
}