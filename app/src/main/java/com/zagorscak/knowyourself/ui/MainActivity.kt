package com.zagorscak.knowyourself.ui

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.zagorscak.knowyourself.KnowYourself
import com.zagorscak.knowyourself.R
import com.zagorscak.knowyourself.databinding.ActivityMainBinding
import pub.devrel.easypermissions.EasyPermissions

class MainActivity : AppCompatActivity(), EasyPermissions.PermissionCallbacks {
    private lateinit var binding: ActivityMainBinding
    private val locationRequestCode = 100
    private val storageRequestCode = 101

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (!hasRecordPermissions() || !hasStoragePermissions()) {
            requestRecordPermission()
            requestStoragePermission()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {}

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {}

    private fun hasRecordPermissions() = EasyPermissions.hasPermissions(
        KnowYourself.application,
        Manifest.permission.RECORD_AUDIO
    )

    private fun hasStoragePermissions() = EasyPermissions.hasPermissions(
        KnowYourself.application,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    private fun requestRecordPermission() {
        EasyPermissions.requestPermissions(
            this,
            "This application cannot record your voice without Record Permission",
            locationRequestCode,
            Manifest.permission.RECORD_AUDIO
        )
    }

    private fun requestStoragePermission() {
        EasyPermissions.requestPermissions(
            this,
            "This application cannot record your voice without Storage Permission",
            storageRequestCode,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
    }

    private fun showToast(message: String) =
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

