package com.zagorscak.knowyourself.ui

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.zagorscak.knowyourself.R
import com.zagorscak.knowyourself.databinding.FragmentSoundPlayerBinding
import com.zagorscak.knowyourself.services.MediaPlayerService
import com.zagorscak.knowyourself.services.MediaPlayerService.Companion.INTENT_EXTRA_EMOTION_KEY
import com.zagorscak.knowyourself.viewmodels.EmotionViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class SoundPlayerFragment : Fragment() {
    private lateinit var binding: FragmentSoundPlayerBinding
    private val viewModel by sharedViewModel<EmotionViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSoundPlayerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            tvSolfeggioFreqs.text = Html.fromHtml(getSolfeggioFreqsTextInHTML())
            viewModel.emotion.observe(viewLifecycleOwner, {
                tvSolfeggioFreqNumber.text = resources.getString(
                    R.string.label_tv_solfeggioFreqNumber,
                    it.solfeggioFrequency.toString()
                )
            })
            updatePlayStopButton(!MediaPlayerService.isServiceRunning())
            ibPlayStop.setOnClickListener {
                YoYo.with(Techniques.FlipInX).playOn(it)
                val isPlaying = MediaPlayerService.isServiceRunning()
                if (isPlaying) stopMediaPlayerService()
                else startMediaPlayerService()
                updatePlayStopButton(isPlaying)
            }
        }
    }

    private fun getSolfeggioFreqsTextInHTML(): String {
        var text = ""
        val freqs = resources.getStringArray(R.array.solfeggioFrequenciesFreqs)
        freqs.forEach {
            text += "<b>" + it.substring(0, 6) + "</b>" + it.substring(6) + "<br/>"
        }
        return text
    }

    private fun startMediaPlayerService() {
        val intent = Intent(activity, MediaPlayerService::class.java)
        intent.putExtra(INTENT_EXTRA_EMOTION_KEY, viewModel.getRecognizedEmotion())
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            activity?.applicationContext?.startForegroundService(intent)
        }
    }

    private fun stopMediaPlayerService() {
        val intent = Intent(activity, MediaPlayerService::class.java)
        activity?.applicationContext?.stopService(intent)
    }

    private fun updatePlayStopButton(isPlaying: Boolean) {
        val resId = if (isPlaying) R.drawable.ic_play else R.drawable.ic_stop
        binding.ibPlayStop.setImageResource(resId)
    }
}