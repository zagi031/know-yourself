package com.zagorscak.knowyourself

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import com.zagorscak.knowyourself.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class KnowYourself : Application() {
    companion object {
        lateinit var application: KnowYourself
        const val CHANNEL_ID = "knowyourselfChannelID"
    }

    override fun onCreate() {
        super.onCreate()
        application = this
        startKoin {
            androidContext(this@KnowYourself)
            modules(viewModelModule)
        }
        createNotificationChannel()
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(CHANNEL_ID, "KnowYourself Service Channel", NotificationManager.IMPORTANCE_DEFAULT)
            val manager = getSystemService(NotificationManager::class.java) as NotificationManager
            manager.createNotificationChannel(serviceChannel)
        }
    }
}